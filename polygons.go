package area_of_polygons

import (
    "fmt"
    "math"
)

func AreaOfPolygons() {
    var sides int64
    fmt.Println("Enter the number of sides \n NOTE: IT SHOULD BE AT LEAST >=3")
    fmt.Scan(&sides)
    if sides < 3 || sides > 12 {
        fmt.Printf("The number of sides (%d) is less than 3 or greater than 12\n", sides)
        return
    }
}

func AreaCalculator(sides int64) float64 {
    switch sides {
    case 3:
        var side_1, side_2, side_3 float64
        fmt.Print("Enter all the sides: ")
        fmt.Scan(&side_1, &side_2, &side_3)
        var s = (side_1 + side_2 + side_3) / 2
        return math.Sqrt(s * (s - side_1) * (s - side_2) * (s - side_3))

    case 4:
        var side_1, side_2 float64
        fmt.Print("Enter the length of all the sides\n Note: This design calculates area for square and rectangle:")
        fmt.Scan(&side_1, &side_2)
        return side_1 * side_2

    case 5:
        var side_1 float64
        fmt.Print("Enter the side length: ")
        fmt.Scan(&side_1)
        return 1.720477400588967 * math.Pow(side_1, 2)

    case 6:
        var side_1 float64
        fmt.Print("Enter the side of the hexagon: ")
        fmt.Scan(&side_1)
        return 2.598076211353316 * math.Pow(side_1, 2)

    case 7:
        var side_1 float64
        angle := math.Pi / 7 // Angle in radians
        fmt.Print("Enter the side of the heptagon: ")
        fmt.Scan(&side_1)
        return (7.0 / 4.0) * math.Pow(side_1, 2) * (1 / math.Tan(angle))

    case 8:
        var side_1 float64
        fmt.Print("Enter the side length: ")
        fmt.Scan(&side_1)
        return 2 * (1 + math.Sqrt(2)) * math.Pow(side_1, 2)

    case 9:
        var side_1 float64
        fmt.Print("Enter the side length: ")
        fmt.Scan(&side_1)
        angle := math.Pi / 9 // Angle in radians
        return (9.0 / 4.0) * math.Pow(side_1, 2) * (1 / math.Tan(angle))
    case 10:
        var side_1 float64
        fmt.Print("Enter the side length: ")
        fmt.Scan(&side_1)
        angle := math.Pi / 10 // Angle in radians
        return (5.0 / 2.0) * math.Pow(side_1, 2) * (1 / math.Tan(angle))

    case 11:
        var side_1 float64
        fmt.Print("Enter the side length: ")
        fmt.Scan(&side_1)
        angle := math.Pi / 11 // Angle in radians
        return (11.0 / 4.0) * math.Pow(side_1, 2) * (1 / math.Tan(angle))

    case 12:
        var side_1 float64
        fmt.Print("Enter the side length: ")
        fmt.Scan(&side_1)
        return 3 * math.Pow(side_1, 2) * math.Sqrt(3)
    }
    return 0
}

